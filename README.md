# PHP-FPM - Ansible Role

Role covers deployment, configuration and software updates of php-fpm and php-modules. 
This role is released under MIT Licence and we give no warranty for this piece of software. 
Currently supported OS - Debian. PHP repository used in the role is coming from: https://packages.sury.org/php/ 

## Variables, versions and modules
`defaults/main.yml` provides example variables that can be used with the role. To modify any of the settings, specify appropriate variables in your `host_vars` file.
To specify php-fpm version, copy and edit `php_version` in your host_vars file. 
To install php modules, specify packages under `php_pkgs` array. For example:
```
php_pkgs:
  - php{{ php_version }}-fpm
  - php{{ php_version }}-bz2
  - php{{ php_version }}-cgi
  - php{{ php_version }}-cli
  - php{{ php_version }}-common
  - php{{ php_version }}-curl
  - php{{ php_version }}-dev
  - php{{ php_version }}-enchant
  - php{{ php_version }}-gd
  - php{{ php_version }}-gmp
  - php{{ php_version }}-igbinary
  - php{{ php_version }}-interbase
  - php{{ php_version }}-intl
  - php{{ php_version }}-mbstring
  - php{{ php_version }}-msgpack
  - php{{ php_version }}-pspell
  - php{{ php_version }}-readline
  - php{{ php_version }}-zip
```
## Supported versions and cleanup
Supported versions of php-fpm:
  - 8.2 (default)
  - 8.1 
  - 8.0

**Note!** 
Role assumes only one version of php should be installed on the host and so it will remove all packages and configuration files other than `{{php_version }}`.


